<?php
/*
Template Name: Rutes
*/

get_header();
?>

<div class="container">
<div class="row">
	<div id="blog-contingut" class="col-sm-8 col-sm-push-4">
		<h1><?php the_title(); ?></h1>
		<table class="table table-hover">
		<?php
			$fields = get_field_objects($post->ID);
			uasort( $fields, 'compare_field_order');
			if($fields):
					foreach( $fields as $field ): ?>
						<?php if(!in_array($field['type'], array('relationship', 'google_map', 'gallery'))): ?>
							<tr>
								<td><strong><?php echo $field['label']; ?></strong></td>
								<td><?php echo $field['value']; ?></td>
							</tr>
						<?php elseif ($field['type'] == 'google_map'): ?>
							<tr>
								<td><strong><?php echo $field['label']; ?></strong></td>
								<td><?php the_field($field['name']); ?></td>
							</tr>
						<?php elseif($field['type'] == 'relationship'): ?>
							<tr>
								<td><strong><?php echo $field['label']; ?></strong></td>
								<td><?php echo count($field['value']); ?></td>
							</tr>
						<?php elseif($field['type'] == 'gallery'): ?>
							<?php $imatges = get_field('fotos_ruta'); ?>
							<?php if($imatges): ?>
							<tr>
								<td><strong><?php echo $field['label']; ?></strong></td>
								<td>
									<?php foreach($imatges as $img): ?>
										<a href="#" class="img-gal">
										<?php echo wp_get_attachment_image($img['id'], 'miniatura'); ?>
										</a>
									<?php endforeach; ?>
								</td>
							</tr>
							<?php endif; ?>
						<?php endif; ?>
					<?php endforeach;
				endif;
		 ?>
		 </table>
	</div>
	<div class="col-sm-4 col-sm-pull-8">
		<?php
			get_sidebar('privada');
		?>
	</div>
</div>
</div>
<div class="modal fade" id="modalimatge" tabindex="-1" role="dialog" aria-labelledby="modalimatge">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title sr-only" id="exampleModalLabel">Imatge de la ruta</h4>
      </div>
      <div class="modal-body">
				<img id="img-modal" class="img-responsive" src="">
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
