<?php
/**
 * Plantilla per als resultats de cerca
 */

get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-push-4">
		<?php if ( have_posts() ) : ?>

			<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'penyaciclista' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>

			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content');
			endwhile;

			penyaciclista_paging_nav();

		else :
			get_template_part( 'template-partscontent', 'none' );

		endif;
		?>
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			<?php get_sidebar(); ?>
		</div>
</div>
</div>


<?php get_footer(); ?>
