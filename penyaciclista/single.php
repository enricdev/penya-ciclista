<?php

get_header(); ?>

<div class="container">
	<div class="row">
	<div class="col-sm-8 col-sm-push-4">
			<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
			<?php endwhile; ?>
			<?php if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>
  	</div>
	<div class="col-sm-4 col-sm-pull-8">
		<?php
			get_sidebar('sidebar-1');
		?>
	</div>
	</div>
</div>

<?php get_footer(); ?>
