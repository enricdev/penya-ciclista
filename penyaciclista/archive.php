<?php
get_header();
?>
<div class="container">
	<div class="col-sm-8 col-sm-push-4">
		<?php if ( have_posts() ) :

			the_archive_title('<h1 class="pag-titol">', '</h1>' );
			the_archive_description( '<div class="taxonomy-description">', '</div>' );

			while ( have_posts() ) : the_post();
				get_template_part('template-parts/content', get_post_format() );
			endwhile;
			
			if(function_exists('paginador_numerat')): paginador_numerat(); endif;
		else :
			get_template_part('template-parts/content', 'none' );
		endif;
		?>
	</div>
	<div class="col-sm-4 col-sm-pull-8">
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>
