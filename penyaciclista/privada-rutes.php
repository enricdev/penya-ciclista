<?php
/*
Template Name: Rutes
*/

get_header();
?>

<div class="container">

	<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(
		'post_type'				=> 'ruta',
		'posts_per_page' => 10,
		'paged' 					=> $paged,
		'meta_key'				=> 'data_eixida',
		'orderby'					=> 'meta_value',
		'order'						=> 'ASC',
		'meta_query' => array(
			array(
				'key' => 'data_eixida',
				'value' => date('Ymd'),
				'type' => 'DATE',
				'compare' => '>'
			)
		),
	);

	$rutes = new WP_Query($args);

	?>
<div class="row">
		<div class="col-sm-8 col-sm-push-4">
			<main id="main" role="main">
			<h1><?php the_title(); ?></h1>
			<?php
			if($rutes->have_posts()): while($rutes->have_posts()): $rutes->the_post();
				get_template_part('template-parts/rutes', 'list');
	 		endwhile;
			paginador_numerat($rutes);
				wp_reset_postdata();

			else:
				echo "No hi ha pròximes rutes previstes";
				endif; ?>
			</main>
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			<?php
				get_sidebar('privada');
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
