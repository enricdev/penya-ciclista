<form role="search" id="searchform" class="hidden-xs navbar-form navbar-right search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<label for="search-form">
			<span class="sr-only"><?php echo _x( 'Search for:', 'label', 'twentyseventeen' ); ?></span>
		</label>
		<input id="search-form" type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'twentyseventeen' ); ?>" value="<?php echo get_search_query(); ?>" name="s">
	</div>
	<button type="submit" class="btn btn-cerca"><span class="sr-only">Submit</span><i class="fa fa-search" aria-hidden="true"></i></button>
</form>
