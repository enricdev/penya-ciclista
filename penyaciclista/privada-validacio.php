<?php
/*
Template Name: Validació d'assistència
*/

get_header();

$currentuser = wp_get_current_user();
$idsoci = get_user_meta($currentuser->ID, 'id_soci', true);

$nomusuari = get_field('nom', $idsoci);
$rutesusuari = get_field('assistencia', $idsoci);
if(!is_array($rutesusuari)):
	$rutesusuari = array();
endif;
?>

<div class="container">
	<?php

	$rutes = get_ruta_hui();
	?>

<div class="row">
	<div class="col-sm-8 col-sm-push-4">
		<main id="main" role="main">
		<h1><?php the_title(); ?></h1>
		<p>Hola <?php echo $nomusuari; ?></p>
		<?php
		if($rutes->have_posts()): while($rutes->have_posts()): $rutes->the_post();
				$assistencia = get_field('assistencia');
				$esmorzar = get_field('esmorzar');
				if(!is_array($assistencia)):
					$assistencia = array();
				endif;
		 	?>
			<p>La ruta de hui és: <?php the_title(); ?></p>
			<p>El lloc d'esmorzar és <strong><?php echo $esmorzar['address']; ?></strong>

			<?php if(!in_array(get_the_ID(), $rutesusuari)): ?>
			<form action="" id="geocoding_form" class="form-horizontal" method="post">
				<input type="hidden" id="latesm" name="latesm" value="<?php echo $esmorzar['lat']; ?>">
				<input type="hidden" id="lngesm" name="lngesm" value="<?php echo $esmorzar['lng']; ?>">
				<div class="form-group">
					<div class="col-xs-12 col-md-6">
						<button type="button" class="find-me btn btn-groc">La meua ubicació</button>
						<p class="coordinates">La teua ubicació coincidix amb el lloc de l'esmorzar. Polsa el botó d'Enviar per a guardar la teua assistència a la ruta.<br>
						Latitude: <b class="latitude"> </b> Longitude: <b class="longitude"> </b></p>
						<input type="submit" id="validaloc" name="validaloc" value="Confirma" class="btn btn-groc" disabled>
					</div>
				</div>
			</form>
			<?php
				if (!empty($_POST)):
					// Guardem la ruta

					$novaruta = array(get_the_ID());
					$rutesmerge = array_merge($rutesusuari, $novaruta);
					$socismerge = array_merge(array($idsoci), $assistencia);

					update_post_meta($idsoci, 'assistencia', $rutesmerge);
					update_post_meta(get_the_ID(), 'assistencia', $socismerge);

				endif;
				?>

		<?php else: ?>
			<p>Has marcat l'assistència a esta ruta</p>
		<?php endif; ?>
 		<?php endwhile;
			wp_reset_postdata();
		else:
			echo "<p>No hi ha cap ruta prevista per a hui</p>";
			endif; ?>
		</main>
	</div>
	<div class="col-sm-4 col-sm-pull-8">
		<?php
			get_sidebar('privada');
		?>
	</div>
</div>
</div>
<?php get_footer(); ?>
