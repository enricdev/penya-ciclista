<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				$comments_number = get_comments_number();
				if ( '1' === $comments_number ) {
					/* translators: %s: post title */
					printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'twentyseventeen' ), get_the_title() );
				} else {
					printf(
						/* translators: 1: number of comments, 2: post title */
						_nx(
							'%1$s Reply to &ldquo;%2$s&rdquo;',
							'%1$s Replies to &ldquo;%2$s&rdquo;',
							$comments_number,
							'comments title',
							'twentyseventeen'
						),
						number_format_i18n( $comments_number ),
						get_the_title()
					);
				}
			?>
		</h2>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'avatar_size' => 100,
					'style'       => 'ol',
					'short_ping'  => true,
					'reply_text'  => '<i class="fa fa-reply" aria-hidden="true"></i>' . __( 'Reply', 'twentyseventeen' ),
				) );
			?>
		</ol>

		<?php the_comments_pagination( array(
			'prev_text' => '<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>' . '<span class="screen-reader-text">' . __( 'Previous', 'twentyseventeen' ) . '</span>',
			'next_text' => '<span class="screen-reader-text">' . __( 'Next', 'twentyseventeen' ) . '</span>' . '<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>',
		) );

	endif; // Check for have_comments().

	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentyseventeen' ); ?></p>
	<?php
	endif;


	$args = array(
		'title_reply'	=> null,
		'comment_notes_before'	=> '<p class="comment-notes">' . __( 'Your email address will not be published.' ).'</p>',
		'comment_field' => is_user_logged_in() ? '<div class="form-group comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" class="form-control" name="comment" cols="45" rows="8" aria-required="true"></textarea></div>' : '',
		'fields' => apply_filters( 'comment_form_default_fields', array(
		 'author' =>
			 '<div class="form-group comment-form-author">' .
			 '<label for="author">' . __( 'Name', 'penyaciclista' ) . '</label> ' .
			 '<input id="author" name="author" class="form-control" type="text" value="' . esc_attr( $commenter['comment_author'] ).'" aria-required="true" />
			 </div>',
			 'email' =>
	      '<div class="form-group comment-form-email"><label for="email">' . __( 'Email', 'penyaciclista' ) . '</label> ' .
	      '<input id="email" name="email" class="form-control" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) .
	      '" aria-required="true"/>
				</div>',
				'comment_field' => '<div class="form-group comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" class="form-control" name="comment" cols="45" rows="8" aria-required="true"></textarea></div>',
			)),
			'class_submit'	=> 'btn btn-default'
	);

		comment_form($args);

	?>

</div><!-- #comments -->
