<?php
/*
Template Name: Informació personal
*/
acf_form_head();
get_header();

$currentuser = wp_get_current_user();
$idsoci = get_user_meta($currentuser->ID, 'id_soci', true);
$assistencia = get_field('assistencia', $idsoci);

?>

<div class="container">
<div class="row">
	<div class="col-sm-8 col-sm-push-4">
		<main id="main" role="main">
		<h1><?php the_title(); ?></h1>
		<button id="edita-btn" class="btn btn-groc">Edita</button>
		<?php if($idsoci): ?>
			<div id="info-pers">
				<table class="table table-hover">
				<?php
					$fields = get_field_objects($idsoci);
					uasort( $fields, 'compare_field_order');
					if($fields):
							foreach( $fields as $field ): ?>
								<?php if(!in_array($field['type'], array('image', 'relationship', 'true_false'))): ?>
									<tr>
										<td><strong><?php echo $field['label']; ?></strong></td>
										<td><?php echo $field['value']; ?></td>
									</tr>
								<?php elseif($field['type'] == 'image'): ?>
									<tr>
										<td><strong><?php echo $field['label']; ?></strong></td>
										<td><?php echo wp_get_attachment_image($field['value']['ID']); ?></td>
									</tr>
								<?php elseif($field['type']	== 'true_false'): ?>
									<?php
										if($field['value'] == 1):
											$val = "Sí";
										else:
											$val = "No";
										endif; ?>
									<tr>
										<td><strong><?php echo $field['label']; ?></strong></td>
										<td><?php echo $val; ?></td>
									</tr>
								<?php endif; ?>
							<?php endforeach;
						endif;
				 ?>
				 	<tr>
						<td><strong>Rutes a les que ha assistit</strong></td>
						<td><?php
						 	foreach($assistencia as $ruta): ?>
								<p><a href="<?php echo get_permalink($ruta); ?>"><?php echo get_the_title($ruta); ?></a></p>
							<?php endforeach;
						?>
						</td>
					</tr>
				 </table>
			</div>
			<div id="edita-pers" class="hidden">
			<?php acf_form(array(
						'post_id'	=> $idsoci,
						'field_groups' => array(58),
						'post_title'	=> false,
						'submit_value'	=> 'Update the post!',
					)); ?>
			</div>
		<?php else: ?>
			<div id="info-pers">
				<p>Completa la teua informació personal</p>
			</div>
			<div id="edita-pers" class="hidden">
			<?php acf_form(array(
						'post_id'	=> 'new_post',
						'post_title'	=> false,
						'submit_value'	=> ''
					)); ?>
			</div>
		<?php endif; ?>
		</main>
	</div>
	<div class="col-sm-4 col-sm-pull-8">
		<?php
			get_sidebar('privada');
		?>
	</div>
</div>
</div>
<?php get_footer(); ?>
