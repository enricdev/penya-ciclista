<?php
	get_header();

$idportada = get_page_by_path('portada');

if( have_rows('contingut', $idportada->ID) ):

     // loop through the rows of data
    while ( have_rows('contingut', $idportada->ID) ) : the_row();

        if( get_row_layout() == 'element_per_a_la_portada' ):
        	if(get_sub_field('element') == 'slider'):
						get_template_part('template-parts/portada', 'slider');
					elseif(get_sub_field('element') == 'noticies'):
						get_template_part('template-parts/portada', 'noticies');
					endif;

        elseif( get_row_layout() == 'pagines_destacades_en_portada' ):

					if(have_rows('pagines_portada')): ?>
						<div id="blocsportada">
							<div class="container">
								<h1 class="h1 sr-only">Pàgines destacades en la portada</h1>
								<div class="row">
						<?php while(have_rows('pagines_portada')): the_row(); ?>
							<?php $pagina = get_sub_field('pagina');
										$imgpag = get_sub_field('imatge');
										$text = get_sub_field('text_resum');
							?>
							<div class="col-md-4">
								<div class="bloc-portada">
								<?php if($imgpag):
									echo wp_get_attachment_image($imgpag['id'], 'page-portada', '', array('class' => 'img-responsive'));
								 endif; ?>
								<h2 class="page-titol"><?php echo get_the_title($pagina->ID); ?></h2>
								<?php if($text): echo "<p>".$text."</p>"; endif; ?>
								<div class="text-center"><a class="btn btn-groc" href="#" role="button">Més info</a></div>
								</div>
							</div>
						<?php endwhile;
						?>
							</div>
						</div>
					</div>
						<?php
					endif;

        endif;

    endwhile;

else:

?>

</div>

<?php endif; // flexible content ?>

<?php get_footer(); ?>
