<?php
/*
Template Name: Classificació
*/

get_header();

$totesrutes = get_posts(
	array(
		'post_type'	=> 'ruta',
		'posts_per_page' => -1
	)
);

$maxrutes = count($totesrutes);
?>

<div class="container">

	<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(
		'post_type' => 'soci',
		'posts_per_page' => 10,
		'paged' => $paged,
		'meta_key'	=> 'assistencia',
		'orderby'	=> 'meta_value'
	);

	$clas = new WP_Query($args);

	?>

<div class="row">
	<div class="col-sm-8 col-sm-push-4">
		<main id="main" role="main">
			<h1><?php the_title(); ?></h1>
			<p><?php echo $post->post_content; ?></p>
			<p>El total de rutes és de <?php echo $maxrutes; ?></p>
			<?php
			if($clas->have_posts()): while($clas->have_posts()): $clas->the_post();
				$assistencia = get_field('assistencia');
				$numrutes = count($assistencia);

				// Calculem la mida que ha de tindre la barra de classificació
				$wdt = ((int)$numrutes*100)/$maxrutes;
			?>

				<div class="class-soci">
					<?php echo wp_get_attachment_image(get_field('foto'), array('50', '50'), '',array('class' => 'img-circle pull-left img-soci')); ?>
					<h2><?php the_title(); ?></h2>
					<div class="progress">
					  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $numrutes; ?>" aria-valuemin="0" aria-valuemax="<?php echo $maxrutes; ?>" style="width: <?php echo $wdt; ?>%;">
					    <span><?php echo $numrutes.'/'.$maxrutes; ?></span>
					  </div>
					</div>
				</div>
				<?php
			endwhile;
				paginador_numerat();
				wp_reset_postdata();
				endif; ?>
		</main>
	</div>
	<div class="col-sm-4 col-sm-pull-8">
		<?php
			get_sidebar('privada');
		?>
	</div>
</div>
</div>
<?php get_footer(); ?>
