<?php
/*
Template Name: Blog
*/

get_header();
?>

<div class="container">

	<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(
		'posts_per_page' => 10,
		'paged' => $paged
	);

	$blog = new WP_Query($args);

	?>
<div class="row">
	<div class="col-sm-8 col-sm-push-4">
		<h1><?php the_title(); ?></h1>
		<?php
		if($blog->have_posts()): while($blog->have_posts()): $blog->the_post();
			get_template_part('template-parts/content', 'list');
 		endwhile;
			paginador_numerat($blog);
			wp_reset_postdata();
			endif; ?>
	</div>
	<div class="col-sm-4 col-sm-pull-8">
		<?php
			get_sidebar('sidebar-1');
		?>
	</div>
</div>
</div>

<?php get_footer(); ?>
