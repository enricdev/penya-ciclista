<?php
get_header();?>

<div class="container">
	<main id="main" role="main">
		<h1 class="pag-titol"><?php the_title();?></h1>
		<div class="contingut">
			<?php if (have_posts()) : while (have_posts()) : the_post();?>
				<?php the_content('<p class="serif">Read the rest of this page »</p>'); ?>
			<?php endwhile; endif; ?>
		</div>
	</main>
</div>

<?php get_footer();?>
