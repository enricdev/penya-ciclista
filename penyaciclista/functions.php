<?php

function penyaciclista_setup() {
	load_theme_textdomain( 'penyaciclista', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

		if ( function_exists( 'add_theme_support' ) ) {
			add_theme_support( 'post-thumbnails' );
			set_post_thumbnail_size( 150, 150 ); // default Post Thumbnail dimensions
	}

		if ( function_exists( 'add_image_size' ) ) {
				add_image_size( 'miniatura', 100, 100, true);
				add_image_size( 'facebook', 843, 400, true );
				add_image_size( 'slider', 1600, 700, true );
				add_image_size( 'post', 800, 400, true );
				add_image_size( 'page-portada', 360, 150, true);
		}
	add_filter( 'use_default_gallery_style', '__return_false' );
	add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'penyaciclista_setup' );


function penyaciclista_scripts() {

	// wp_enqueue_style( $handle, $src, $dependencies, $versio, $media );

	wp_enqueue_style( 'penyaciclista-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '3.3.6' );
	wp_enqueue_style( 'penyaciclista-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', array());
	wp_enqueue_style( 'penyaciclista-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.css', array());
	wp_enqueue_style( 'penyaciclista-style', get_stylesheet_uri() );

	// wp_enqueue_script( $handle, $src, $dependencies, $versio, $in_footer );

	wp_enqueue_script( 'penyaciclista-bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '20151204', true );
	wp_enqueue_script( 'penyaciclista-owl-js', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'penyaciclista-js', get_template_directory_uri() . '/assets/js/penyaciclista.js', array( 'jquery' ), '', true );

}
add_action( 'wp_enqueue_scripts', 'penyaciclista_scripts' );


require get_parent_theme_file_path( '/inc/post-types.php');
require get_parent_theme_file_path( '/inc/funcions.php');
require get_parent_theme_file_path( '/inc/personalitzador.php');
require get_parent_theme_file_path( '/inc/widgets.php');
require get_parent_theme_file_path( '/inc/acf-mod.php');
require get_parent_theme_file_path( '/inc/cordova.php');
require get_parent_theme_file_path( '/inc/seguretat.php');
