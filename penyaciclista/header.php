<?php
// header("Access-Control-Allow-Origin: *");

$home = home_url();
$blogname = get_bloginfo('blogname');
$siteicon = get_option('logo_s');
$socialimg = get_option('socialimg_s');


if(empty($siteicon)):
	$siteicon = get_bloginfo('template_directory').'/assets/img/logo.png';
endif;

if(!is_front_page()):
	$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'facebook');
	$urlimg = $image[0];
	$stitol = $post->post_title;
	$sdescripcio = wp_trim_words($post->post_content, 40);
	$simatge = $urlimg;
	$urlpagina = get_the_permalink();
else:
	$stitol = $blogname;
	$sdescripcio = get_bloginfo('description');
	$simatge = $socialimg;
	$urlpagina = $home;
endif;

?>

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php wp_head(); ?>

		<meta name="theme-color" content="#4A4A4A">

		<meta property="og:title" content="<?php echo $stitol; ?>"/>
		<meta property="og:description" content="<?php echo $sdescripcio; ?>">
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="<?php echo $blogname; ?>"/>
		<meta property="og:url" content="<?php echo $urlpagina; ?>" />
		<meta property="fb:app_id" content="" />
		<meta property="og:image" content="<?php echo $simatge; ?>" />
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:title" content="<?php echo $stitol; ?>">
		<meta name="twitter:description" content="<?php echo $sdescripcio; ?>">
		<meta name="twitter:image:src" content="<?php echo $simatge; ?>">

	</head>
	<body <?php if(is_front_page() ) { echo ' id="homepage"'; }?> <?php body_class(); ?>>
	<div id="cookies" data-text="Aquesta web utilitza cookies propies i de tercers per a oferir una millor experiència a l'usuari. Al navegar esteu acceptat el seu ús. Podeu desactivar les cookies canviant la configuració del vostre navegador." data-ok="Acceptar" data-politica="Més informació" data-url="#"></div>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#penyaciclista-navbar" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo $home; ?>"><img src="<?php echo $siteicon; ?>" alt="logo"></a>
					</div>
					<div class="xarxes">
						<?php if(is_user_logged_in()): ?>
							<a class="login strong" href="<?php echo get_permalink(get_page_by_path('area-privada/rutes')); ?>">Àrea de socis</a>
						<?php else: ?>
							<a class="login strong" href="<?php echo wp_login_url(); ?>">Inicia sessió</a>
						<?php endif; ?>
						<span class=" hidden-xs">
							<a href="#"><i class="fa fa-youtube"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-facebook-official"></i></a>
						</span>
					</div>
					<div id="penyaciclista-navbar" class="collapse navbar-collapse">
						<?php
							wp_nav_menu( array(
								'theme_location' 		=> 'header-menu',
								'container'         => '',
		            'menu_class'        => 'nav navbar-nav',
		            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		            'walker'            => new wp_bootstrap_navwalker()
							) );
						?>
					</div>
				</div>
			</nav>
		</header>

		<div id="pagina">
