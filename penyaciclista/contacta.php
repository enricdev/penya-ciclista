<?php
/*
Template Name: Contacta
*/
get_header(); ?>

<div class="container">
	<h1>Contacta amb nosaltres</h1>
	<div class="row">
		<div class="col-sm-6">
			<form>
				<div class="form-group">
					<label for="nom">El teu nom: </label>
					<input type="text" class="form-control" id="nom" placeholder="Nom">
				</div>
				<div class="form-group">
					<label for="email">Adreça de correu: </label>
					<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">@</span><input type="email" class="form-control" id="correu" placeholder="Correu electrònic">
					</div>
				</div>
				<div class="form-group">
					<label for="missatge">El teu missatge: </label>
					<textarea class="form-control"></textarea>
				</div>
				<button type="submit" class="btn btn-groc">Enviar</button>
			</form>

		</div>
		<div class="col-sm-6">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1537.3094285331747!2d-0.5448178822297319!3d39.59073958153488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sca!2ses!4v1428828920071" width="100%" height="400" frameborder="0" style="border:0"></iframe>
		</div>
	</div>
</div>
<?php get_footer();?>
