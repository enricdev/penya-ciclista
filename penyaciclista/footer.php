<?php

$facebook = get_option('facebook_s');
$twitter = get_option('twitter_s');
$mail = get_option('mail_s');

?>
		</div> <!-- Pagina -->
			<footer>
				<div class="container">
					<div class="row">
						<div class="col-sm-3 hidden-xs">
							<img class="ajuntament" src="<?php bloginfo('template_directory'); ?>/assets/img/logos/ajuntament.png" alt="ajuntament de la pobla de vallbona">
							<img class="esport" src="<?php bloginfo('template_directory'); ?>/assets/img/logos/construimesportbn.png" alt="construim esport">
							<img class="diputacio" src="<?php bloginfo('template_directory'); ?>/assets/img/logos/diputaciodevalencia.png" alt="Diputació de València">
						</div>
						<div class="col-sm-3">
							<p class="titolpeu">Penya Ciclista <br>Pobla de Vallbona</p>
							<p><i class="fa fa-map-marker"></i> C/València sn, 46185 <br>La Pobla de Vallbona (València)</p>
							<p><i class="fa fa-phone"></i> 622 065 548</p>
							<p><i class="fa fa-envelope"></i> info@pcpobla.com</p>
						</div>
						<div class="col-sm-3 text-center">
							<p class="h4">Descarrega la nostra app</p>
							<a href="/wp-content/uploads/apk/android-debug.apk" target="_blank">
								<img class="gplay img-responsive" src="<?php bloginfo('template_directory'); ?>/assets/img/logos/en_badge_web_generic.png" alt="Google Play">
							</a>
						</div>
						<div class="col-sm-3">
							<div class="xarxes">
							<a href="#"><i class="fa fa-youtube"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-facebook-official"></i></a>
							</div>
							<div class="linkspeu">
								<?php wp_nav_menu( array(
									'theme_location' => 'footer-menu',
									'items_wrap' => '<ul class="nav text-sm-right">%3$s</ul>',
								 ) ); ?>
							</div>
						</div>
					</div>
				</div>
			</footer>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<?php wp_footer(); ?>
		<script>
			localitzacio();
		</script>
	</body>
</html>
