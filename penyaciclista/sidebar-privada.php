<?php if ( is_active_sidebar( 'sidebar-privada' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-privada' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
